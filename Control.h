#ifndef _H_CONTROL
#define _H_CONTROL

#include <SFML/Graphics.hpp>
#include "Player.h"

class Player;

class Control
{
public:
	Control();
	void input(Player &gracz);
	~Control();
};

#endif //_H_CONTROL