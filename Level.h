#pragma once

#include <SFML/Graphics.hpp>
#include "Platform.h"
#include <iostream>
#include <vector>
#define TileSize 32.f

class Platform;

class Level
{
public:
	Level();
	void rysuj(sf::RenderTarget& target);
	void setPlatform(int x, int y, int size);
	std::vector <Platform>& getPlatform() { return platformy; }
	~Level();
private:
	std::vector <Platform> platformy;
	sf::Texture tekstura;
};

