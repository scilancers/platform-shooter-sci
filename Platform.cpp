#include "Platform.h"
#define TileSize 32.f


Platform::Platform(sf::Vector2f _position, int _size, sf::Texture& tekstura)
	:
	m_size(_size),
	m_position(_position)
{
	//Left side//
	sf::Sprite Left(tekstura, sf::IntRect(0, 0, TileSize, TileSize));
	Left.setPosition(_position);
	tileset.push_back(Left);

	//Middle//
	if (m_size > 2)
	{
		sf::Sprite Middle(tekstura, sf::IntRect(1*TileSize, 0, TileSize, TileSize));
		for (unsigned char i = 2; i < m_size; i++)
		{
			_position.x += TileSize;
			Middle.setPosition(_position);
			tileset.push_back(Middle);
		}
	}

	_position.x += TileSize;

	//Right side//
	sf::Sprite Right(tekstura, sf::IntRect(64, 0, TileSize, TileSize));
	Right.setPosition(_position);
	tileset.push_back(Right);
}

bool Platform::kolizja(Player _gracz)
{
	sf::Vector2f _pos = _gracz.sprajt.getPosition();

	if (_pos.y > m_position.y && _pos.y < m_position.y + (1 * TileSize) &&
		_pos.x > m_position.x && _pos.x < m_position.x + (m_size * TileSize))
	{	
		if (fall)
		{
			std::cout << "spada w bloku" << std::endl;
			return false;
		}
		else
		{
			std::cout << "kolizja" << std::endl;
			return true;
		}
					
	}
	else
	{	
		if (fall == true)
			std::cout << "Ustawione na true Zmienia na false" << std::endl;
		fall = false;
		return false;	}
}

void Platform::turnDown()
{
	std::cout << "true" << std::endl;
	fall = true;
}

Platform::~Platform()
{
}
