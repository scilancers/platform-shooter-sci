#ifndef _H_PLAYER
#define _H_PLAYER

#include <SFML/Graphics.hpp>
#include "Control.h"
#include "Level.h"
#include "Platform.h"
#include <vector>
#include <iostream>

class Platform;

enum Direction {
	LEFT = 1,
	RIGHT = 2,
	JUMP,
	DOWN
};

class Player : sf::Drawable
{
public:
	Player();
	void action(Direction akcja);
	void upDate(std::vector <Platform>& platformy);
	void kolizja(std::vector <Platform>& platformy);
	~Player();
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states = sf::RenderStates::Default) const
	{ target.draw(sprajt); }

	sf::Sprite sprajt;
private:
	sf::Texture tekstura;
	float speed;
	float EarthAccel;
	sf::Vector2f velocity;
	bool isGrounded;
	bool isPressDown;
};

#endif //_H_CONTROL