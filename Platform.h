#pragma once

#include <SFML/Graphics.hpp>
#include "Player.h"
#include <vector>
#include <iostream>

class Player;

class Platform : sf::Drawable
{
public:
	///@pragma sf::Vector2f _position - pozycja od gracza
	Platform( sf::Vector2f _position, int _size, sf::Texture& tekstura);
	bool kolizja(Player _gracz);
	sf::Vector2f getPos() { return m_position; }
	void turnDown();
	~Platform();
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states = sf::RenderStates::Default) const
	{
		for (size_t i = 0; i < tileset.size(); i++)
			target.draw(tileset[i]);
	}
private:
	std::vector <sf::Sprite> tileset;
	bool fall = false;
	int m_size;
	sf::Vector2f m_position;
};

