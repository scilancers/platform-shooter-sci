#include "Level.h"


//25x19 mapa

Level::Level()
{
	//Loading texture//
	if (!tekstura.loadFromFile("platform.png"))
	{
		std::cerr << "ERROR 404: Brak tekstury platformy!" << std::endl;
	}

	setPlatform(0, 18, 25);
	setPlatform(15, 15, 5);
	setPlatform(14, 14, 2);
}

void Level::setPlatform(int x, int y, int size)
{
	Platform base(sf::Vector2f(x*TileSize, y*TileSize), size, tekstura);
	platformy.push_back(base);
}

void Level::rysuj(sf::RenderTarget& target)
{
	for (size_t i = 0; i < platformy.size(); i++)
		platformy[i].draw(target);
}

Level::~Level()
{
}
