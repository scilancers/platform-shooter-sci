#include "Player.h"
#define tileSize 32


Player::Player()
	:
	sprajt			(tekstura, sf::IntRect(0, -1, 32, 48)),
	speed			(40.f),
	EarthAccel		(60.f),
	isGrounded		(false),
	isPressDown		(false)
{
	if (!tekstura.loadFromFile("player.png"))
	{
		std::cerr << "ERROR, blad tekstury playera" << std::endl;
	}

	sprajt.setOrigin(int(sprajt.getTextureRect().width) / 2, sprajt.getTextureRect().height);
	sprajt.setPosition(2* tileSize, 18*tileSize);
}

void Player::action(Direction akcja)
{
	if (akcja == LEFT)
	{
		velocity.x -= speed / 60.f;
	}
	if (akcja == RIGHT)
	{
		velocity.x += speed / 60.f;
	}
	if (akcja == JUMP && isGrounded)
	{
		velocity.y = -20.f;
		isGrounded = false;
	}
	if (akcja == DOWN)
	{
		isPressDown = true;
	}
}

void Player::upDate(std::vector <Platform>& platformy)
{
	//grawitacja//
	velocity.y += EarthAccel / 60.f;

	sprajt.move(velocity);

	kolizja(platformy);

	velocity = { velocity.x * 0.9f,velocity.y * 0.98f }; //x = walking braking & y = air resistance
	
}

void Player::kolizja(std::vector <Platform>& platformy) //kolizja z platform�
{
	for (size_t i = 0; i < platformy.size(); i++)
	{
		if (platformy[i].kolizja(*this) && velocity.y >= 0)
		{
			if (isPressDown)
			{
				platformy[i].turnDown();
				isPressDown = false;
				continue;
			}

			isGrounded = true;
			velocity.y = 0.f;
			sprajt.setPosition(sprajt.getPosition().x, platformy[i].getPos().y);
		}
	}
}


Player::~Player()
{
	speed = NULL;
	EarthAccel = NULL;
	isGrounded = NULL;
	isPressDown = NULL;
}
