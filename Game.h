#pragma once

#include <SFML/Graphics.hpp>
#include "Level.h"
#include "Player.h"
#include "Online.h"
#include "PlayerOnline.h"
#include <vector>

class Game
{
public:
	Game();
	void run();
	void rysuj();
	void EventManager();
	void OneFrame();
	~Game();
private:
	sf::RenderWindow window;
	sf::Clock clock;
	sf::Time timeSinceLastUpdate;
	sf::Time TimePerFrame;
	sf::Event event;
    sf::Vector2f posMP;
	Level mapa;
	Player gracz;
    PlayerOnline graczOnline;
	Control sterowanie;
    Online konekcja;
};

