//Game by Jakub Kleban
//jakub.kleban@o2.pl
//537 500 763

#include <iostream>
#include <SFML/Graphics.hpp>
#include "Game.h"

int main()
{
	Game gra;
	gra.run();

	return 0;
}