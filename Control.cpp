#include "Control.h"



Control::Control()
{
}

Control::~Control()
{
}

void Control::input(Player& gracz)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		gracz.action(Direction::LEFT);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		gracz.action(Direction::RIGHT);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) || sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		gracz.action(Direction::JUMP);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		gracz.action(Direction::DOWN);
}


