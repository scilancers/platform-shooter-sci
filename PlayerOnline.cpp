#include "PlayerOnline.h"
#define tileSize 32


PlayerOnline::PlayerOnline(sf::Vector2f pozycja)
	:
	sprajt(tekstura, sf::IntRect(0, -1, 32, 48))
{
	if (!tekstura.loadFromFile("player.png"))
	{
		std::cerr << "ERROR 404: blad wczytywania tekstury playeronline" << std::endl;
	}

	sprajt.setOrigin(int(sprajt.getTextureRect().width) / 2, sprajt.getTextureRect().height);
	sprajt.setPosition(pozycja);
}

bool PlayerOnline::upDate(sf::Vector2f pozycja)
{
	if (sprajt.getPosition() == pozycja)
		return false;
	else
	{
		sprajt.setPosition(pozycja);
		return true;
	}
}