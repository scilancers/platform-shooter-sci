//
// Created by dominikoso on 06.04.17.
//

#ifndef PLATFORM_SHOOTER_SCI_ONLINE_H
#define PLATFORM_SHOOTER_SCI_ONLINE_H

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <iostream>


class Online {
public:
    //Variables
    const unsigned short PORT = 31137;
    const std::string IPADDRESS = "localhost";
    sf::Vector2f dataSend;
    sf::TcpSocket socket;
    sf::Mutex globalMutex;
    //sf::Thread* thread = new sf::Thread(&initialize);
    char who;
    sf::Vector2f msg;


    //Methods
    Online();
    ~Online();
    void initialize();
    void Server();
    bool Client();
    void ctrlThread();
    void getData(sf::Vector2f position);
    sf::Vector2f getMsg();


};


#endif //PLATFORM_SHOOTER_SCI_ONLINE_H
