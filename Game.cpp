#include "Game.h"



Game::Game()
	:
	window					(sf::VideoMode(800, 608), "PlatformShooter Game", sf::Style::Close),
	timeSinceLastUpdate		(sf::Time::Zero),
	TimePerFrame			(sf::seconds(1.0f / 60.0f)),
	mapa					(),
    posMP                   (600, 400),
	gracz					(),
    graczOnline             (posMP),
	sterowanie				(),
	konekcja				()
{
	window.setFramerateLimit(60);
}


Game::~Game()
{
}

void Game::run()
{
	//Pêtla czasu rzeczywistego
	while (window.isOpen())
	{
		EventManager();

        konekcja.initialize();
        konekcja.getData(gracz.sprajt.getPosition());
        posMP = konekcja.getMsg();

		OneFrame();

		rysuj();
	}
}

void Game::rysuj()
{
	window.clear(sf::Color::Cyan);

	mapa.rysuj(window);
	gracz.draw(window);
    graczOnline.draw(window);

	window.display();
}

void Game::EventManager()
{
	//obs³uga zdarzeñ
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed || (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape))
			window.close();
	}
}

void Game::OneFrame()
{
	timeSinceLastUpdate += clock.restart();
	while (timeSinceLastUpdate > TimePerFrame)
	{
		timeSinceLastUpdate -= TimePerFrame;

		sterowanie.input(gracz);
		gracz.upDate(mapa.getPlatform());
        graczOnline.upDate(posMP);
	}
}