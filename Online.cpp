//
// Created by dominikoso on 06.04.17.
//

#include "Online.h"

Online::Online()
{

    std::cout << "Do you want to be a server (s) or a client (c) ? ";
    std::cin  >> who;

    if(who == 's')
        Server();
    else
        Client();


}
Online::~Online()
{

}

void Online::initialize()
{
    static sf::Vector2f oldData;

        sf::Packet packetSendX;
        sf::Packet packetSendY;
        globalMutex.lock();
        packetSendX << dataSend.x;
        packetSendY << dataSend.y;
        globalMutex.unlock();

        socket.send(packetSendX);
        sf::Packet packetReceiveX;
        socket.receive(packetReceiveX);

        socket.send(packetSendY);
        sf::Packet packetReceiveY;
        socket.receive(packetReceiveY);


        if(packetReceiveX >> msg.x && packetReceiveY >> msg.y)
        {
            if(oldData != msg) {

                std::cout << msg.x << " " << msg.y << std::endl;
                oldData = msg;
            }
        }
    }


void Online::Server()
{
    sf::TcpListener listener;
    listener.listen(PORT);
    listener.accept(socket);
    std::cout << "New client connected: " << socket.getRemoteAddress() << std::endl;
}
bool Online::Client()
{
    if(socket.connect(IPADDRESS, PORT) == sf::Socket::Done)
    {
        std::cout << "Connected\n";
        return true;
    }
    return false;
}
void Online::ctrlThread()
{
  //  if(thread)
    //{
      //  thread->wait();
        //delete thread;
    //}

}
void Online::getData(sf::Vector2f position)
{
    sf::Vector2f s;
    //std::cout << "Loading Data" << std::endl;
    s = position;
    globalMutex.lock();
    dataSend = s;
    globalMutex.unlock();
}
sf::Vector2f Online::getMsg() {return msg;}