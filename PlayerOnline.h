#ifndef _H_PLAYERONLINE
#define _H_PLAYERONLINE

#include <SFML/Graphics.hpp>
#include "Online.h"
#include <iostream>

class PlayerOnline : sf::Drawable
{
public:
	PlayerOnline(sf::Vector2f pozycja);
	bool upDate(sf::Vector2f pozycja);
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states = sf::RenderStates::Default) const
	{
		target.draw(sprajt);
	}
private:
	sf::Sprite sprajt;
	sf::Texture tekstura;
};

#endif //_H_PLAYERONLINE